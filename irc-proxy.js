var irc = require('irc');
var amqp = require('amqplib/callback_api');


var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_client;
var id;

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;

		inicializar();
	});
});

function inicializar () {

	receberDoCliente("registro_conexao", function (msg) {

		id      = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;

		irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);

		irc_client.addListener('message'+canal, function (from, message) {
		    
		    console.log(from + ' => '+ canal +': ' + message);
		    
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(), 
				"nick": from,
				"msg": message
			});
		});

		irc_client.addListener('error', function(message) {
		    console.log('error: ', message);
		});

		irc_client.addListener('motd', function(message) {
		    console.log('motd: ', message);
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(), 
				"nick": 'Servidor',
				"msg": message
			});
		});


		proxies[id] = irc_client;
	});

	receberDoCliente("gravar_mensagem", function (msg) {
		// console.log(msg);

		// var canal = msg.msg.split('/join');
		// console.log("------------- "+canal[0]+" ---------");
		// console.log('Canal[0]: '+ canal[0]+"\nCanal[1]"+ canal[1]);
		// var nick = msg.msg.split('/nick');
		console.log("irc msg:"+ msg.msg);
		var cmd = msg.msg.split(" ");
		if (msg.msg.charAt(0) == '/'){
			console.log("\n CMD[0]:"+cmd[0]+"\n");
			switch(cmd[0]){
				case'/motd':
					irc_client.send("motd");
				break;

				case '/quit':
					console.log("teste");
					irc_client.send("quit");
					irc_client.disconnect();
					enviarParaCliente(id, {
		    			"timestamp": Date.now(), 
						"nick": 'servidor',
						"msg": '/quitOK'
					});	
					delete proxies[irc_client.id];
					delete irc_client;
				break;	
			
				case '/join':	
					irc_client.join(cmd[1], 'txt');
					enviarParaCliente(id, {
		    			"timestamp": Date.now(), 
						"nick": 'servidor',
						"msg": '/joinOK',
						"canal": cmd[1]
					});

				break;

				case '/nick':
					console.log('Trocando nick');
					irc_client.send("nick", cmd[1]);
					enviarParaCliente(id, {
		    			"timestamp": Date.now(), 
						"nick": 'servidor',
						"msg": '/nickOK',
						"nick": cmd[1]
					});	
				break;

	
			}
			return;
		}
		console.log("Que canal é:"+msg.canal);
		irc_client.say(msg.canal, msg.msg);
	});
}

function receberDoCliente (canal, callback) {

	amqp_ch.assertQueue(canal, {durable: false});

    console.log(" [irc] Waiting for messages in ", canal);
    
    amqp_ch.consume(canal, function(msg) {

      console.log(" [irc] Received %s", msg.content.toString());
      callback(JSON.parse(msg.content.toString()));

    }, {noAck: true});
}

function enviarParaCliente (id, msg) {

	msg = new Buffer(JSON.stringify(msg));

	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
	console.log(" [irc] Sent %s", msg);
}




